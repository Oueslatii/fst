import { InitResetPasswordComponent } from './views/reset-password/init-reset-password/init-reset-password.component';
import { AuthComponent } from './views/auth/auth.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './layout/full/pages.component';
import { HomeComponent } from './views/home/home.component';
import { GestionUtilisateurComponent } from './views/administration/gestion-utilisateur/gestion-utilisateur.component';
import { ResetPasswordComponent } from './views/reset-password/reset-password.component';

const routes: Routes = [
  {
    path : 'login',
    component : AuthComponent,
    pathMatch : 'full'
  },
  {
    path: '',
    component: AuthComponent,
  },
  {
    path : 'home',
    component : HomeComponent,
  },
  {
    path : 'reset-init',
    component : ResetPasswordComponent,
  },
  {
    path : 'init-reset-password',
    component : InitResetPasswordComponent
  },
  {
    path : 'accueil',
    component : PagesComponent,
    children : [
      {
        path : 'user',
        component : GestionUtilisateurComponent,
      }
    ],
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
