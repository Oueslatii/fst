import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from 'src/app/services/lang.service';

@Component({
  selector: 'app-pages-top',
  templateUrl: './pages-top.component.html',
  styleUrls: ['./pages-top.component.css']
})
export class PagesTopComponent implements OnInit {

  constructor(private router: Router, private translate: TranslateService, private langService: LangService) { }
  langue: string = 'Français';
  ngOnInit() {
    let x = localStorage.getItem('langue');
    if (x === 'en') {
      this.langue = 'English';
    }
    else if (x === 'fr') {
      this.langue = 'Français';
    }
    else if (x === 'ar') {
      this.langue = 'العربية';
    }

  }
  seDisconnect() {
    this.router.navigate(['/']);
  }

  changeLang(e) {
    let x = '';
    if (e === 'Arabe' || e === 'Arab' || e === 'لعربية') {
      x = 'ar';
    }
    if (e === 'Anglais' || e === 'English' || e === 'الإنجليزية') {
      x = 'en';
    }
    if (e === 'Français' || e === 'Frensh' || e === 'الفرنسية') {
      x = 'fr';
    }
    this.translate.use(x);
    localStorage.setItem('langue', x);
    this.langService.filter(x);
  }
}
