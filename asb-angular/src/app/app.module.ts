import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesComponent } from './layout/full/pages.component';
import { PagesTopComponent } from './layout/pages-top/pages-top.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './views/auth/auth.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HomeComponent } from './views/home/home.component';
import { GestionUtilisateurComponent } from './views/administration/gestion-utilisateur/gestion-utilisateur.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ResetPasswordComponent } from './views/reset-password/reset-password.component';
import { InitResetPasswordComponent } from './views/reset-password/init-reset-password/init-reset-password.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// import ngx-translate and the http loader
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    PagesTopComponent,
    AuthComponent,
    HomeComponent,
    GestionUtilisateurComponent,
    SidebarComponent,
    ResetPasswordComponent,
    InitResetPasswordComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/');
}
