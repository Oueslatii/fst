import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'GROUPE ADAMING SOLUTION';

  browserLanguage: string;
  constructor(private translate: TranslateService) {
    let y = localStorage.getItem('langue');
    if (y === '' || y == null) {
      translate.setDefaultLang(navigator.language.substring(0, 2));
      localStorage.setItem('langue', navigator.language.substring(0, 2));
    }
    else translate.setDefaultLang(y);
}
}
