import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LangService {
  private listners = new Subject<any>();
  constructor(private http: HttpClient) {
  }
  public getFR(): Observable<any> {
    return this.http.get('../assets/lang/fr.json');
  }
  public getEN(): Observable<any> {
    return this.http.get('../assets/lang/en.json');
  }
  public getAR(): Observable<any> {
    return this.http.get('../assets/lang/ar.json');
  }


  listen(): Observable<any> {
    return this.listners.asObservable();
 }

 filter(filterBy: string) {
    this.listners.next(filterBy);
 }
}
