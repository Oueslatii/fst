import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppUtils } from '../app-utils';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private jwtToken;
  private urlPath: string;
  constructor(private http: HttpClient) {
     this.urlPath = AppUtils.getServerAbsolutePath();
  }

  login(user) {
    return this.http.post(this.urlPath + '/login', user, {observe : 'response'});
  }

  saveToken(jwt) {
    localStorage.setItem('token', jwt);
  }
  loadToken(jwtToken) {
    this.jwtToken = jwtToken;
    this.saveToken(this.jwtToken);
  }
  logout() {
    this.jwtToken = null;
    localStorage.removeItem('token');
  }
}
