import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppUtils } from '../app-utils';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlPath: string;
  constructor(private http: HttpClient) {
    this.urlPath = AppUtils.getServerAbsolutePath() + '/api';
  }
  getAll() {
    return this.http.get(this.urlPath + '/users', {
      headers: new HttpHeaders({ Authorization: localStorage.getItem('token') })
    });
  }
  getById(idUser: number) {
    return this.http.get(this.urlPath + '/user/' + idUser, {
      headers: new HttpHeaders({ Authorization: localStorage.getItem('token') })
    });
  }

  getByLogin(login: string) {
    return this.http.get(
      this.urlPath + '/userByLogin?login=' + login,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.getItem('token')
        })
      }
    );
  }
}
