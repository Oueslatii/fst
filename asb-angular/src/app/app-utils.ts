export class AppUtils {
  static getServerAbsolutePath() {
    let SEPARATOR: string = '/';
    let url: string;
    let protocol: string = window.location.protocol;
    let host: string = window.location.host;
    let port: string = window.location.port;
    let projectPath = window.location.pathname.split(SEPARATOR)[1];
    url = protocol + SEPARATOR + SEPARATOR + host + SEPARATOR + projectPath;
    return url;
  }
}
