import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  submitted = false;
  user = { login: null, password: null };
  constructor(private router: Router, private formBuilder: FormBuilder,private authService: AuthService, private userService: UserService) { }
  ngOnInit() {
    this.authForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }
  get f() {
     return this.authForm.controls;
  }
  onSubmit() {
    this.submitted = true;
        // stop here if form is invalid
    if (this.authForm.invalid) {
          return;
        }
    this.authService.login(this.user).subscribe(response => {
    let jwtToken = response.headers.get('Authorization');
    localStorage.setItem('token', jwtToken);
    this.authService.loadToken(jwtToken);
    this.userService.getByLogin(this.user.login).subscribe((resp: any) => {
      console.log('login ok');
            });
    this.router.navigate(['/accueil']);
  });
}
    // reinitialiser ma mot de passe
  resetPassword() {
      this.router.navigate(['/init-reset-password']);
  }
}
