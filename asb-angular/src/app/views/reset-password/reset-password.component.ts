import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  user = { login: null, password: null, resetKey: null, confirmPassword: null };
  submitted = false;
  resetForm: FormGroup;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      confirmpassword: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() {
    return this.resetForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (
      this.resetForm.invalid ||
      this.user.password !== this.user.confirmPassword
    ) {
      return;
    }
    const routeParams = this.activeRoute.snapshot.params;
    if (routeParams !== null) {
      this.user.login = String(routeParams.login);
      this.user.resetKey = String(routeParams.resetKey);
    }
    this.router.navigate(['/login']);
  }
}
